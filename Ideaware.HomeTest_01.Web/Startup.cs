﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ideaware.HomeTest_01.Web.Startup))]
namespace Ideaware.HomeTest_01.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }

}
