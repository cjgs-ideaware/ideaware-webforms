﻿using Ideaware.HomeTest_01.Core;
using Ideaware.HomeTest_01.Core.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ideaware.HomeTest_01.Web
{
    public partial class _Default : Page
    {
        string connectionString = GetDefaultConnectionString();
        private IUnitOfWork unitOfWork;
        private IIndividualService service;
        private List<int> toMergeIds = new List<int>();

        protected override void OnInit(EventArgs e)
        {
            unitOfWork = new UnitOfWork(connectionString);
            service = new IndividualService(unitOfWork);
            ObjectDataSource1.ObjectCreating += ObjectDataSource1_ObjectCreating;
            base.OnInit(e);
        }

        private void ObjectDataSource1_ObjectCreating(object sender, System.Web.UI.WebControls.ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = service;
        }

        public override void Dispose()
        {
            unitOfWork.Dispose();
            base.Dispose();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("/Account/Login");
            }
        }

        protected void BtnMerge_Click(Object sender, EventArgs e)
        {
            service.Merge(service.GetFull().Join(toMergeIds, x => x.Id, y => y, (x, y) => x));
            toMergeIds.Clear();
            GridView1.DataBind();
        }

        protected void ChkSelectedIndividual_CheckedChanged(Object sender, EventArgs e)
        {
            var checkbok = (CheckBox)sender;
            var id = int.Parse(checkbok.Attributes["data-id"]);
            toMergeIds.Add(id);
            checkbok.Checked = false;
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            BulletedList list;
            if (null != (list = (BulletedList)e.Row.FindControl("BulletedList1")) 
                && e.Row.DataItem != null)
            {
                list.DataSource = ((Core.Entities.Individual)e.Row.DataItem).Addresses;
            }
        }

        public static string GetDefaultConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }
    }
}