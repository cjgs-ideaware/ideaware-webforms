﻿using Ideaware.HomeTest_01.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Test
{
    [TestClass]
    public class DataTest : BaseTest
    {
        [TestMethod]
        public async Task BulkData()
        {
            var individualService = new IndividualService(UnitOfWork);
            var inidividuals = Newtonsoft.Json.JsonConvert.DeserializeObject<Entities.Individual[]>(Properties.Resources.MOCK_DATA);
            await individualService.BulkDataAsync(inidividuals);
        }

        [TestMethod]
        public async Task CreateIndividual()
        {
            var created = await UnitOfWork.IndividualRepository.CreateAsync(new Entities.Individual
            {
                FirstName = $"Test",
                LastName = $"{Guid.NewGuid()}",
                Gender = Entities.Gender.Female,
            });
            Assert.IsTrue(created.Id > 0);
        }

        [TestMethod]
        public async Task CreateIndividualAndAddress()
        {
            var individualService = new IndividualService(UnitOfWork);
            Entities.Individual createdIndividual;
            Entities.Address createdAddress;
            Entities.Address createdAddress2;

            await individualService.BulkDataAsync(new Entities.Individual[] {
                createdIndividual = new Entities.Individual
                {
                    FirstName = $"Test",
                    LastName = $"{Guid.NewGuid()}",
                    Gender = Entities.Gender.Female,
                    Addresses = new Entities.Address[]{
                        createdAddress = new Entities.Address{
                            City = $"City_{Guid.NewGuid()}",
                            State = $"State_{Guid.NewGuid()}",
                            StreetName = $"StreeName{Guid.NewGuid()}",
                        },
                        createdAddress2 = new Entities.Address{
                            City = $"City_{Guid.NewGuid()}",
                            State = $"State_{Guid.NewGuid()}",
                            StreetName = $"StreeName{Guid.NewGuid()}",
                        }
                    }
                }
            });
            Assert.IsTrue(createdIndividual.Id > 0);
            Assert.IsTrue(createdAddress.Id > 0);
            Assert.IsTrue(createdAddress2.Id > 0);
        }

        [TestMethod]
        public async Task MergeIndividuals()
        {
            var individualService = new IndividualService(UnitOfWork);
            var individuals = await individualService.GetFullAsync();
            individuals = individuals.Where(x => x.Addresses?.Any() == true);
            await individualService.MergeAsync(individuals.Take(3));
        }
    }
}
