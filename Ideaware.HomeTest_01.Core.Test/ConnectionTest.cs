﻿using Ideaware.HomeTest_01.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Test
{
    [TestClass]
    public class ConnectionTest
    {
        const string connectionString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=Ideaware.HomeTest_01.DB;Integrated Security=True;";

        [TestMethod]
        public async Task Read()
        {
            using (IUnitOfWork unitOfWork = new UnitOfWork(connectionString))
            {
                IIndividualService individualService = new IndividualService(unitOfWork);
                var list = await individualService.GetFullAsync();
            }
        }
    }
}
