﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ideaware.HomeTest_01.Core.Test
{
    [TestClass]
    public class BaseTest
    {
        //const string connectionString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=Ideaware.HomeTest_01.DB;Integrated Security=True;";
        const string connectionString = @"Data Source=tcp:ideaware.database.windows.net,1433;Initial Catalog=ideaware;User Id=cjaviersaldana@ideaware.database.windows.net;Password=`e^^hj=zlCM\s9&sE?3j;";


        public UnitOfWork UnitOfWork { get; private set; }

        [TestInitialize]
        public void Init()
        {
            this.UnitOfWork = new UnitOfWork(connectionString);
        }

        [TestCleanup]
        public void Dispose()
        {
            UnitOfWork.Dispose();
        }
    }
}
