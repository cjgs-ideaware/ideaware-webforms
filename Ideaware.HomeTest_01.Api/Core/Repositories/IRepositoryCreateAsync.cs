﻿using Ideaware.HomeTest_01.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public interface IRepositoryCreateAsync<T> where T: Dto
    {
        Task<T> CreateAsync(T toCreate);
        Task<IEnumerable<T>> CreateAsync(IEnumerable<T> toCreate);
    }
}
