﻿using Ideaware.HomeTest_01.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public interface IRepositoryReadAsync<T> where T : Dto
    {
        Task<IEnumerable<T>> ReadAsync();
        Task<T> ReadAsync(int id);
        Task<IEnumerable<T>> ReadAsync(Expression<Func<T, bool>> predicate);
    }
}
