﻿using Ideaware.HomeTest_01.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public interface IIndividualRepository : IRepositoryAsync<Individual>
    {
        Task<bool> LinkToAddress(Individual individual);
        Task<bool> LinkToAddress(IEnumerable<Individual> individual);
    }
}
