﻿using Ideaware.HomeTest_01.Core.Entities;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public interface IRepositoryAsync<T> :
        IRepositoryCreateAsync<T>, IRepositoryDeleteAsync<T>,
        IRepositoryUpdateAsync<T>, IRepositoryReadAsync<T>
        where T : Dto
    {

    }
}
