﻿using Ideaware.HomeTest_01.Core.Entities;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public interface IRepositoryDeleteAsync<T> where T: Dto
    {
        Task<bool> DeleteAsync(T toDelete);
        Task<bool> DeleteAsync(int id);
    }
}
