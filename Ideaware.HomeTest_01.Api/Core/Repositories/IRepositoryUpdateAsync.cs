﻿using Ideaware.HomeTest_01.Core.Entities;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public interface IRepositoryUpdateAsync<T> where T: Dto
    {
        Task<bool> UpdateAsycn(T toUpdate);
    }
}
