﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ideaware.HomeTest_01.Core.Entities;

namespace Ideaware.HomeTest_01.Core.Services
{
    public interface IIndividualService
    {
        Task<bool> BulkDataAsync(IEnumerable<Individual> data);
        Task<IEnumerable<Individual>> GetFullAsync();
        IEnumerable<Individual> GetFull();
        Task<bool> MergeAsync(IEnumerable<Individual> toMerge);
        bool Merge(IEnumerable<Individual> toMerge);
    }
}
