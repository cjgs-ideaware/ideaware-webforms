﻿using Ideaware.HomeTest_01.Core.Repositories;
using System;

namespace Ideaware.HomeTest_01.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IIndividualRepository IndividualRepository { get; }
        IAddressRepository AddressRepository { get; }
    }
}
