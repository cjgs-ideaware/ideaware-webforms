﻿using System.Collections.Generic;

namespace Ideaware.HomeTest_01.Core.Entities
{
    public class Address : Dto
    {
        public string StreetName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public IEnumerable<Individual> Individuals { get; set; } = new Individual[] { };
    }
}
