﻿using System.Collections.Generic;

namespace Ideaware.HomeTest_01.Core.Entities
{
    public class Individual : Dto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public IEnumerable<Address> Addresses { get; set; } = new Address[] { };
    }
}
