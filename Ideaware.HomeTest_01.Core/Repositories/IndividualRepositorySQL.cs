﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Ideaware.HomeTest_01.Core.Entities;
using Ideaware.HomeTest_01.Core.Providers;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public class IndividualRepositorySQL : BaseRepositorySQL<Individual>, IIndividualRepository
    {
        public IndividualRepositorySQL(DbContext dbContext) : base(dbContext)
        {
        }

        public async override Task<Individual> CreateAsync(Individual toCreate)
        {
            toCreate.Id = await DbContext.ExecuteScalarAsync<int>(Properties.Resources.Individual_Insert,
                new SqlParameter("FirstName", SqlDbType.NVarChar, toCreate.FirstName.Length) { Value = toCreate.FirstName },
                new SqlParameter("LastName", SqlDbType.NVarChar, toCreate.LastName.Length) { Value = toCreate.LastName },
                new SqlParameter("Gender", (int)toCreate.Gender)
                );
            return toCreate;
        }

        public override Task<bool> DeleteAsync(Individual toDelete)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> LinkToAddress(Individual individual)
        {
            var tasks = individual
                .Addresses
                .Select(address => DbContext.ExecuteNonQueryAsync(
                    Properties.Resources.Individual_Link,
                    new SqlParameter("Individual_Id", individual.Id),
                    new SqlParameter("Address_Id", address.Id)));

            return (await Task.WhenAll(tasks)).Select(x => x > 0).Distinct().Count() == 1;
        }

        public async Task<bool> LinkToAddress(IEnumerable<Individual> individual)
        {
            bool result = true;
            foreach (var item in individual)
            {
                result &= await LinkToAddress(item);
            }
            return result;
        }

        public override Task<IEnumerable<Individual>> ReadAsync()
        {
            return DbContext.ExecuteQueryToIEnumerableAsync(Properties.Resources.Individual_Select,
                (column) => new Individual
                {
                    Id = column.Field<int>("Individual_Id"),
                    FirstName = column.Field<string>("FirstName"),
                    LastName = column.Field<string>("LastName"),
                    Gender = (Gender)column.Field<byte>("Gender"),
                    Addresses = GetListFromXml(column.Field<string>("Addresses"))
                });
        }

        private IEnumerable<Address> GetListFromXml(string strXml)
        {
            if (string.IsNullOrWhiteSpace(strXml))
            {
                return new Address[] { };
            }

            var document = new XmlDocument();
            document.LoadXml(strXml);

            return document
                .SelectNodes("Addresses/Address")
                .Cast<XmlElement>()
                .Select(x => new Address
                {
                    Id = int.Parse(x.Attributes["Id"].Value)
                });
        }

        public override Task<bool> UpdateAsycn(Individual toUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
