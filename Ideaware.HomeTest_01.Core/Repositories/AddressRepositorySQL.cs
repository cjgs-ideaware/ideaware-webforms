﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Xml;
using Ideaware.HomeTest_01.Core.Entities;
using Ideaware.HomeTest_01.Core.Providers;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public class AddressRepositorySQL : BaseRepositorySQL<Address>, IAddressRepository
    {
        public AddressRepositorySQL(DbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<Address> CreateAsync(Address toCreate)
        {
            toCreate.Id = await DbContext.ExecuteScalarAsync<int>(Properties.Resources.Address_Insert,
                new SqlParameter("StreetName", toCreate.StreetName),
                new SqlParameter("City", toCreate.City),
                new SqlParameter("State", toCreate.State)
                );
            return toCreate;
        }

        public override Task<bool> DeleteAsync(Address toDelete)
        {
            throw new System.NotImplementedException();
        }

        public override Task<IEnumerable<Address>> ReadAsync()
        {
            return DbContext.ExecuteQueryToIEnumerableAsync(Properties.Resources.Address_Select,
                (column) => new Address
                {
                    Id = column.Field<int>("Address_Id"),
                    City = column.Field<string>("City"),
                    State = column.Field<string>("State"),
                    StreetName = column.Field<string>("StreetName"),
                    //Individuals = column.Field<XmlDocument>("Individuals")
                });
        }

        public override Task<bool> UpdateAsycn(Address toUpdate)
        {
            throw new System.NotImplementedException();
        }
    }
}
