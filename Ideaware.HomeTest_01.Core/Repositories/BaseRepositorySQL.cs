﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Ideaware.HomeTest_01.Core.Entities;
using Ideaware.HomeTest_01.Core.Providers;

namespace Ideaware.HomeTest_01.Core.Repositories
{
    public abstract class BaseRepositorySQL<T>
        : IRepositoryAsync<T>
        where T : Dto
    {
        private readonly DbContext dbContext;

        public BaseRepositorySQL(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        protected DbContext DbContext => dbContext;

        public abstract Task<T> CreateAsync(T toCreate);

        public virtual Task<IEnumerable<T>> CreateAsync(IEnumerable<T> toCreate)
        {
            return Task.Run(() => (IEnumerable<T>)toCreate
                .AsParallel()
                .Select(x =>
                {
                    var task = CreateAsync(x);
                    task.Wait();
                    return task.Result;
                })
                .ToList());
        }

        public abstract Task<bool> DeleteAsync(T toDelete);

        public async Task<bool> DeleteAsync(int id)
        {
            var toDelete = await ReadAsync(id);
            return await DeleteAsync(toDelete);
        }

        public abstract Task<IEnumerable<T>> ReadAsync();

        public async virtual Task<T> ReadAsync(int id)
        {
            return (await ReadAsync()).FirstOrDefault(x => x.Id == id);
        }

        public async virtual Task<IEnumerable<T>> ReadAsync(Expression<Func<T, bool>> predicate)
        {
            return (await ReadAsync())
                .AsQueryable()
                .Where(predicate: predicate);
        }

        public abstract Task<bool> UpdateAsycn(T toUpdate);
    }
}
