﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Ideaware.HomeTest_01.Core.Providers
{
    public class DbContext : IDisposable
    {
        private SqlConnectionStringBuilder connectionString;
        private Lazy<SqlConnection> connection;

        public DbContext(string connectionString)
        {
            this.connectionString = new SqlConnectionStringBuilder(connectionString)
            {
                ConnectTimeout = 0,
                MultipleActiveResultSets = true,
            };
            connection = new Lazy<SqlConnection>(() =>
            {
                var con = new SqlConnection(this.connectionString.ToString());
                con.Open();
                return con;
            });
        }

        public Task<int> ExecuteNonQueryAsync(string query, params SqlParameter[] sqlParams)
        {
            return ExecuteCommandAsync(query, async (command) =>
            {
                return await command.ExecuteNonQueryAsync();
            }, sqlParams);
        }

        public Task<T> ExecuteScalarAsync<T>(string query, params SqlParameter[] sqlParams) where T : struct
        {
            return ExecuteCommandAsync(query, async (command) =>
            {
                return (T)Convert.ChangeType(await command.ExecuteScalarAsync(), typeof(T));
            }, sqlParams);
        }

        public Task<IEnumerable<T>> ExecuteQueryToIEnumerableAsync<T>(string query, Func<DataRow, T> parser, params SqlParameter[] sqlParams)
        {
            return ExecuteCommandAsync(query, async (command) =>
            {
                using (var reader = await command.ExecuteReaderAsync())
                {
                    if (!reader.HasRows)
                    {
                        return new T[] { };
                    }
                    using (var table = new DataTable())
                    {
                        table.Load(reader);
                        return (IEnumerable<T>)table
                            .Select()
                            .AsParallel()
                            .Select(x => parser(x))
                            .ToArray();
                    }
                }
            }, sqlParams);
        }

        public async Task ExecuteCommandAsync(string query, Func<SqlCommand, Task> bodyCommand, params SqlParameter[] sqlParams)
        {
            using (var command = connection.Value.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = query;
                if (sqlParams?.Any() == true)
                {
                    command.Parameters.AddRange(sqlParams);
                }
                await bodyCommand(command);
            }
        }

        public async Task<T> ExecuteCommandAsync<T>(string query, Func<SqlCommand, Task<T>> bodyCommand, params SqlParameter[] sqlParams)
        {
            using (var command = connection.Value.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = query;
                if (sqlParams?.Any() == true)
                {
                    command.Parameters.AddRange(sqlParams);
                }
                return await bodyCommand(command);
            }
        }

        public void Dispose()
        {
            if (connection.IsValueCreated)
            {
                connection.Value.Close();
                connection.Value.Dispose();
            }
        }
    }
}
