﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ideaware.HomeTest_01.Core.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Ideaware.HomeTest_01.Core.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO [Ideawaredbo].[Address]
        ///           ([StreetName]
        ///           ,[City]
        ///           ,[State])
        ///	 OUTPUT 
        ///			inserted.[Address_Id]
        ///     VALUES
        ///           (@StreetName
        ///           ,@City
        ///           ,@State);
        ///.
        /// </summary>
        internal static string Address_Insert {
            get {
                return ResourceManager.GetString("Address_Insert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT [Address_Id]
        ///      ,[StreetName]
        ///      ,[City]
        ///      ,[State]
        ///  FROM [Ideawaredbo].[Address].
        /// </summary>
        internal static string Address_Select {
            get {
                return ResourceManager.GetString("Address_Select", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO [Ideawaredbo].[Individual]
        ///           ([FirstName]
        ///           ,[LastName]
        ///           ,[Gender])
        ///	 OUTPUT 
        ///			inserted.[Individual_Id]
        ///     VALUES
        ///           (@FirstName
        ///           ,@LastName
        ///           ,@Gender).
        /// </summary>
        internal static string Individual_Insert {
            get {
                return ResourceManager.GetString("Individual_Insert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IF NOT EXISTS(SELECT TOP(1)
        ///  1
        ///FROM Ideawaredbo.IndividualAddress T
        ///WHERE T.Address_Id = @Address_Id and T.Individual_Id = @Individual_Id)
        ///BEGIN
        ///  INSERT INTO [Ideawaredbo].[IndividualAddress]
        ///    ([Individual_Id]
        ///    ,[Address_Id])
        ///  VALUES
        ///    (@Individual_Id
        ///           , @Address_Id)
        ///END.
        /// </summary>
        internal static string Individual_Link {
            get {
                return ResourceManager.GetString("Individual_Link", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT [Individual_Id]
        ///      ,[FirstName]
        ///      ,[LastName]
        ///      ,[Gender]
        ///  FROM [Ideawaredbo].[Individual].
        /// </summary>
        internal static string Individual_Select {
            get {
                return ResourceManager.GetString("Individual_Select", resourceCulture);
            }
        }
    }
}
