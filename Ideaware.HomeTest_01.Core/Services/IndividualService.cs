﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ideaware.HomeTest_01.Core.Entities;
using Ideaware.HomeTest_01.Core.Tools;

namespace Ideaware.HomeTest_01.Core.Services
{
    public class IndividualService : IIndividualService
    {
        private readonly IUnitOfWork unitOfWork;

        public IndividualService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> BulkDataAsync(IEnumerable<Individual> data)
        {
            var addresses = data
                .AsParallel()
                .SelectMany(x => x.Addresses)
                .ToArray();

            var addressesCreateTask = unitOfWork.AddressRepository.CreateAsync(addresses);
            var individualsCreateTask = unitOfWork.IndividualRepository.CreateAsync(data);

            await Task.WhenAll(addressesCreateTask, individualsCreateTask);

            return await unitOfWork.IndividualRepository.LinkToAddress(data);
        }

        public async Task<IEnumerable<Individual>> GetFullAsync()
        {
            var addressesReadTask = unitOfWork.AddressRepository.ReadAsync();
            var individualsReadTask = unitOfWork.IndividualRepository.ReadAsync();

            await Task.WhenAll(addressesReadTask, individualsReadTask);

            var addressParallel = (await addressesReadTask).AsParallel();

            return (await individualsReadTask)
                .AsParallel()
                .Select(individual =>
                {
                    individual.Addresses = individual.Addresses
                        .Join(addressParallel, x => x.Id, y => y.Id, (x, y) => y)
                        .ToArray();
                    return individual;
                }).ToArray();
        }

        public IEnumerable<Individual> GetFull()
        {
            return AsyncHelper.RunSync(() => GetFullAsync()); ;
        }

        public Task<bool> MergeAsync(IEnumerable<Individual> toMerge)
        {
            var addresses = toMerge
                .AsParallel()
                .SelectMany(x => x.Addresses)
                .Distinct()
                .ToArray();

            toMerge
                .AsParallel()
                .ForAll(individual => individual.Addresses = addresses);

            return unitOfWork.IndividualRepository.LinkToAddress(toMerge);
        }

        public bool Merge(IEnumerable<Individual> toMerge)
        {
            return AsyncHelper.RunSync(() => MergeAsync(toMerge));
        }
    }
}
