﻿using System;
using Ideaware.HomeTest_01.Core.Providers;
using Ideaware.HomeTest_01.Core.Repositories;

namespace Ideaware.HomeTest_01.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private Lazy<DbContext> dbContext;
        private Lazy<IIndividualRepository> individualRepository;
        private Lazy<IAddressRepository> addressRepository;

        public UnitOfWork(string connectionString)
        {
            dbContext = new Lazy<DbContext>(() => new DbContext(connectionString));
            individualRepository = new Lazy<IIndividualRepository>(() => new IndividualRepositorySQL(dbContext.Value));
            addressRepository = new Lazy<IAddressRepository>(() => new AddressRepositorySQL(dbContext.Value));
        }

        public IIndividualRepository IndividualRepository => individualRepository.Value;

        public IAddressRepository AddressRepository => addressRepository.Value;

        public void Dispose()
        {
            if (dbContext.IsValueCreated)
            {
                dbContext.Value.Dispose();
            }
        }
    }
}
