﻿IF NOT EXISTS(SELECT TOP(1)
  1
FROM Ideawaredbo.IndividualAddress T
WHERE T.Address_Id = @Address_Id and T.Individual_Id = @Individual_Id)
BEGIN
  INSERT INTO [Ideawaredbo].[IndividualAddress]
    ([Individual_Id]
    ,[Address_Id])
  VALUES
    (@Individual_Id
           , @Address_Id)
END