﻿INSERT INTO [Ideawaredbo].[Individual]
           ([FirstName]
           ,[LastName]
           ,[Gender])
	 OUTPUT 
			inserted.[Individual_Id]
     VALUES
           (@FirstName
           ,@LastName
           ,@Gender)