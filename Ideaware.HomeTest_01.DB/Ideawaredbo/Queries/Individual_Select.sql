﻿SELECT
  A.[Individual_Id]
  , A.[FirstName]
  , A.[LastName]
  , A.[Gender]
  , (SELECT Address_Id [Id]
     FROM Ideawaredbo.IndividualAddress T
     WHERE T.Individual_Id = A.[Individual_Id]
     FOR XML RAW('Address'), ROOT('Addresses')) [Addresses]
FROM [Ideawaredbo].[Individual] A