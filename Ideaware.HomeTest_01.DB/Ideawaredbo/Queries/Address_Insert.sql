﻿INSERT INTO [Ideawaredbo].[Address]
           ([StreetName]
           ,[City]
           ,[State])
	 OUTPUT 
			inserted.[Address_Id]
     VALUES
           (@StreetName
           ,@City
           ,@State);
