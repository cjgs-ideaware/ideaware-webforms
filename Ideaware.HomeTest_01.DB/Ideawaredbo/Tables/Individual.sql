﻿CREATE TABLE [Ideawaredbo].[Individual]
(
	[Individual_Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NCHAR(50) NOT NULL, 
    [Gender] TINYINT NOT NULL
)
