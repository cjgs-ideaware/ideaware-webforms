﻿CREATE TABLE [Ideawaredbo].[Address]
(
	[Address_Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [StreetName] NVARCHAR(100) NOT NULL, 
    [City] NVARCHAR(50) NOT NULL, 
    [State] NVARCHAR(50) NOT NULL
)
