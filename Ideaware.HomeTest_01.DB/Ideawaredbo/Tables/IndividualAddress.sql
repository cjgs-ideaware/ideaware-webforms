﻿CREATE TABLE [Ideawaredbo].[IndividualAddress]
(
	[Individual_Id] INT NOT NULL , 
    [Address_Id] INT NOT NULL, 
    PRIMARY KEY ([Individual_Id], [Address_Id]), 
    CONSTRAINT [FK_IndividualAddress_To_Individual] FOREIGN KEY ([Individual_Id]) REFERENCES [Ideawaredbo].[Individual]([Individual_Id]), 
    CONSTRAINT [FK_IndividualAddress_To_Address] FOREIGN KEY ([Address_Id]) REFERENCES [Ideawaredbo].[Address]([Address_Id])
)
